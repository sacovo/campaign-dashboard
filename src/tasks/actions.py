from django.contrib.auth.decorators import login_required, permission_required
from django.http.response import JsonResponse

from dashboard.utils import Action, LoginRequiredAction
from tasks.forms import SelectUserForm
from tasks.models import DONE


class JoinTask(LoginRequiredAction):
    def form_valid(self, form):
        user = self.request.user
        self.obj.participants.add(user)

        return JsonResponse({'task_id': self.obj.pk, 'status': 'joined'})


class LeaveTask(LoginRequiredAction):
    def form_valid(self, form):
        user = self.request.user
        self.obj.participants.remove(user)

        return JsonResponse({'task_id': self.obj.pk, 'status': 'left'})


class CloseTask(LoginRequiredAction):
    def form_valid(self, form):
        self.obj.state = DONE
        self.obj.save()
        return JsonResponse({'task_id': self.obj.pk, 'status': 'done'})

    @permission_required('tasks.change_task')
    def process(self, request, *args, **kwargs):
        return super().process(request, *args, **kwargs)


class RemoveUserFromTask(LoginRequiredAction):
    form = SelectUserForm

    def form_valid(self, form):
        self.obj.participants.remove(self.data.user)

        return JsonResponse({
            'task_id': self.obj.pk,
            'status': 'removed',
            'user_id': self.data.user.id
        })

    @permission_required('tasks.change_task')
    def process(self, request, *args, **kwargs):
        return super().process(request, *args, **kwargs)
