from django.contrib import admin

from tasks.models import Task, Topic
from modeltranslation.admin import TranslationAdmin

# Register your models here.


@admin.register(Task)
class TaskAdmin(TranslationAdmin):
    fields = [
        'title', 'description', 'deadline', 'issuer', 'participants',
        'rocket_chat'
    ]


@admin.register(Topic)
class TopicAdmin(TranslationAdmin):
    pass
