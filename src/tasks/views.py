from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from dashboard.utils import ActionView
from tasks.actions import CloseTask, JoinTask, LeaveTask, RemoveUserFromTask
from tasks.models import Task

# Create your views here.


class TaskList(ListView):
    model = Task
    paginate_by = 20


class TaskDetail(DetailView):
    model = Task


class TaskActionView(ActionView):
    model = Task

    actions = {
        'join': JoinTask,
        'leave': LeaveTask,
        'close': CloseTask,
        'remove': RemoveUserFromTask
    }
