from modeltranslation.translator import TranslationOptions, translator

from tasks.models import Task, Topic


class TopicTranslation(TranslationOptions):
    fields = ('name', )


class TaskTranslation(TranslationOptions):
    fields = ('title', 'description')


translator.register(Topic, TopicTranslation)
translator.register(Task, TaskTranslation)
