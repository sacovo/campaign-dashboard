from django import forms
from django.contrib.auth import get_user_model


class SelectUserForm(forms.Form):
    user = forms.ModelChoiceField(get_user_model().objects.all())
