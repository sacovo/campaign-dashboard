from ckeditor.fields import RichTextField
from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

DRAFT = 0
PUBLISHED = 3
DONE = 5

TASK_STATES = (
    (DRAFT, _("draft")),
    (PUBLISHED, _("published")),
    (DONE, _("done")),
)


class Topic(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Task(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    deadline = models.DateTimeField()

    title = models.CharField(max_length=255)
    description = RichTextField()

    issuer = models.ForeignKey(get_user_model(), models.CASCADE)

    state = models.IntegerField(choices=TASK_STATES, default=DRAFT)

    topics = models.ManyToManyField(Topic, blank=True)

    participants = models.ManyToManyField(get_user_model(),
                                          related_name="assigned_task_set",
                                          blank=True)

    rocket_chat = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created_at']
