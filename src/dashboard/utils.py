"""This module contains per object actions that can be called with some data."""

from django.contrib.auth.decorators import login_required
from django.forms.forms import Form
from django.http.response import Http404, JsonResponse
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.views.generic.detail import SingleObjectMixin


class Action:
    """
    Action are defined through a form and an action.

    Action is registered under a name that has to be submitted
    as `action` parameter, e. g. {'action': 'do_something'}.

    The post data is then processed through a form, defined by
    the action. If the form is valid the action is executed with
    the data from the form.

    If not, the error handler is called.

    """
    form = Form
    name: str = ''

    def __init__(self, request):
        self.request = request

    def get_form_dict(self):
        return self.request.POST

    def get_form_class(self):
        """Return the form class"""
        return self.form

    def get_form(self):
        """Return filled form"""
        return self.get_form_class()(self.get_form_dict())

    def form_valid(self, form):
        """Perform actions based on the valid form."""
        raise NotImplementedError(
            "form_valid(self, form) must be implemented.")

    def form_invalid(self, form):
        return JsonResponse({'errors': form.errors, 'action': self.name})

    def get_queryset(self, queryset):
        return queryset

    def set_object(self, obj):
        self.obj = obj

    def process(self, request, *args, **kwargs):
        """Process the request and return a response."""
        form = self.get_form()

        if form.is_valid():
            self.data = form.cleaned_data
            return self.form_valid(form)
        return self.form_invalid(form)


@method_decorator(login_required, name='process')
class LoginRequiredAction(Action):
    def process(self, request, *args, **kwargs):
        return super().process(self, request, *args, **kwargs)


class ActionView(SingleObjectMixin, View):
    """This view handles post actions. Each action has a name and requires some data."""
    actions = {}

    def setup(self, request, *args, **kwargs):
        return super().setup(request, *args, **kwargs)

    def get_active_action(self):
        name = self.request.GET.get('action')
        if name is None:
            raise Http404("no action specified")
        return name

    def get(self, request, *args, **kwargs):
        """Actions can be post or get."""
        return self.hanlde()

    def post(self, request, *args, **kwargs):
        """Actions can be post or get."""
        return self.handle()

    def get_action(self, name):
        action = self.actions.get(name)
        if action is None:
            raise Http404("action %s not found", params={'action': name})
        return action

    def handle(self):
        name = self.get_active_action()
        action = self.get_action(name)(self.request)
        # Use the actions queryset so that an action can restrict access.
        action.set_object(
            self.get_object(action.get_queryset(self.get_queryset()))),
        return action.process(self.request)
