from django.urls import path
from django.urls.conf import include

from dashboard.views import UserAutoComplete, account, dashboard_view

app_name = "dashboard"

urlpatterns = [
    path("", dashboard_view, name="home"),
    path("account/", account, name="account"),
    path("users/", UserAutoComplete.as_view(), name="user-autocomplete"),
]
