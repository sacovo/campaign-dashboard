from modeltranslation.translator import TranslationOptions, translator

from dashboard.models import Application


class ApplicationOptions(TranslationOptions):
    fields = ('name', 'description')


translator.register(Application, ApplicationOptions)
