from dal import autocomplete
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.db.models import Value as V
from django.db.models.expressions import F, Value
from django.db.models.functions import Concat
from django.db.models.query_utils import Q
from django.shortcuts import render
from django.utils.html import escape

from events.models import Event
from tasks.models import PUBLISHED, Task

# Create your views here.


@login_required
def dashboard_view(request):

    return render(
        request, 'dashboard/home.html', {
            'tasks':
            Task.objects.filter(participants=request.user, state=PUBLISHED),
            'events':
            Event.objects.filter(
                Q(owner=request.user) | Q(managers=request.user))
        })


@login_required
def account(request):
    return render(request, "dashboard/account.html", {})


class UserAutoComplete(autocomplete.Select2QuerySetView):
    def get_result_label(self, result):
        return escape(f"{result.first_name} {result.last_name}")

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return get_user_model().objects.none()

        if self.q and len(self.q) > 2:
            return get_user_model().objects.annotate(
                full_name=Concat("first_name", V(" "), "last_name")).filter(
                    full_name__icontains=self.q).exclude(
                        pk=self.request.user.pk).order_by('last_name')

        return get_user_model().objects.none()
