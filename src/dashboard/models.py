from django.db import models
from django.utils.translation import ugettext_lazy as _
from ordered_model.models import OrderedModel

# Create your models here.


class Application(OrderedModel):
    """Application that activists can access with their login."""
    url = models.URLField(verbose_name=_("url"))

    name = models.CharField(max_length=255, verbose_name=_('name'))
    description = models.TextField(verbose_name=_("description"))
    image = models.ImageField(upload_to='apps/', verbose_name=_("image"))

    class Meta(OrderedModel.Meta):
        verbose_name = _("application")
        verbose_name_plural = _("applications")

    def __str__(self):
        return self.name
