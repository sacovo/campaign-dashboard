# Generated by Django 3.1.7 on 2021-03-01 21:07

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id',
                 models.AutoField(auto_created=True,
                                  primary_key=True,
                                  serialize=False,
                                  verbose_name='ID')),
                ('order',
                 models.PositiveIntegerField(db_index=True,
                                             editable=False,
                                             verbose_name='order')),
                ('url', models.URLField(verbose_name='url')),
                ('name', models.CharField(max_length=255,
                                          verbose_name='name')),
                ('name_de',
                 models.CharField(max_length=255,
                                  null=True,
                                  verbose_name='name')),
                ('name_fr',
                 models.CharField(max_length=255,
                                  null=True,
                                  verbose_name='name')),
                ('name_it',
                 models.CharField(max_length=255,
                                  null=True,
                                  verbose_name='name')),
                ('name_en',
                 models.CharField(max_length=255,
                                  null=True,
                                  verbose_name='name')),
                ('description',
                 ckeditor.fields.RichTextField(verbose_name='description')),
                ('description_de',
                 ckeditor.fields.RichTextField(null=True,
                                               verbose_name='description')),
                ('description_fr',
                 ckeditor.fields.RichTextField(null=True,
                                               verbose_name='description')),
                ('description_it',
                 ckeditor.fields.RichTextField(null=True,
                                               verbose_name='description')),
                ('description_en',
                 ckeditor.fields.RichTextField(null=True,
                                               verbose_name='description')),
                ('image',
                 models.ImageField(upload_to='apps/', verbose_name='image')),
            ],
            options={
                'verbose_name': 'application',
                'verbose_name_plural': 'applications',
                'ordering': ('order', ),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NewsItem',
            fields=[
                ('id',
                 models.AutoField(auto_created=True,
                                  primary_key=True,
                                  serialize=False,
                                  verbose_name='ID')),
                ('title', models.CharField(max_length=255,
                                           verbose_name='title')),
                ('title_de',
                 models.CharField(max_length=255,
                                  null=True,
                                  verbose_name='title')),
                ('title_fr',
                 models.CharField(max_length=255,
                                  null=True,
                                  verbose_name='title')),
                ('title_it',
                 models.CharField(max_length=255,
                                  null=True,
                                  verbose_name='title')),
                ('title_en',
                 models.CharField(max_length=255,
                                  null=True,
                                  verbose_name='title')),
                ('content', models.TextField(verbose_name='content')),
                ('content_de',
                 models.TextField(null=True, verbose_name='content')),
                ('content_fr',
                 models.TextField(null=True, verbose_name='content')),
                ('content_it',
                 models.TextField(null=True, verbose_name='content')),
                ('content_en',
                 models.TextField(null=True, verbose_name='content')),
                ('image',
                 models.ImageField(upload_to='news/', verbose_name='image')),
                ('created_at',
                 models.DateTimeField(auto_now_add=True,
                                      verbose_name='created')),
            ],
        ),
    ]
