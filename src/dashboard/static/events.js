window.addEventListener("load", () => {
  (function () {
    htmx.on("htmx:load", function (evt) {
      feather.replace();
      document.title = evt.detail.elt.dataset.title || document.title;
    });
  })();

  setTimeout(() => {
    document.title =
      document.getElementById("pageContent").dataset.title || document.title;
  }, 0);
});
