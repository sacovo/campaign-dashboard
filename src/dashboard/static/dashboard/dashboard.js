window.addEventListener("load", () => {
  (function () {
    const sidePanel = document.getElementById("sidePanel");
    let isSmallScreen = window.innerWidth < 576;

    function closeNavigation() {
      sidePanel.setAttribute("data-open", false);
    }

    htmx.on("htmx:load", function (evt) {
      isSmallScreen = window.innerWidth < 576;
      if (isSmallScreen) {
        closeNavigation();
      }
      feather.replace();
      document.title = evt.detail.elt.dataset.title || document.title;
    });

    document.getElementById("menuToggle").addEventListener("click", () => {
      if (sidePanel.getAttribute("data-open") === "true") {
        sidePanel.setAttribute("data-open", false);
      } else {
        sidePanel.setAttribute("data-open", true);
      }
    });

    if (isSmallScreen) {
      closeNavigation();
    }

    feather.replace();
  })();

  setTimeout(() => {
    document.title =
      document.getElementById("pageContent").dataset.title || document.title;
  }, 0);
});
