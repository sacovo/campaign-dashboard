from dashboard.models import Application


def application_context(request):
    if request.user.is_authenticated:
        return {'applications': Application.objects.all()}
    return {}
