from django.contrib import admin

from dashboard.models import Application
from modeltranslation.admin import TranslationAdmin

# Register your models here.


@admin.register(Application)
class ApplicationAdmin(TranslationAdmin):
    list_display = ['name', 'url']
    search_fields = ['name']
