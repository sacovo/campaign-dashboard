from django.urls.conf import path

from news.views import NewsItemDetail, NewsItemList

app_name = "news"

urlpatterns = [
    path("<int:pk>/", NewsItemDetail.as_view(), name="newsitem_detail"),
    path("", NewsItemList.as_view(), name="newsitem_list"),
]
