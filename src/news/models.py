from ckeditor.fields import RichTextField
from django.db import models
from django.utils.translation import ugettext_lazy as _


class NewsItem(models.Model):
    """News item"""
    title = models.CharField(max_length=255, verbose_name=_("title"))
    content = RichTextField(verbose_name=_("content"))

    image = models.ImageField(upload_to='news/', verbose_name=_("image"))

    created_at = models.DateTimeField(verbose_name=_("created"),
                                      auto_now_add=True)

    def __str__(self):
        return self.title
