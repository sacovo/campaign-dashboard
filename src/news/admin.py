from django.contrib import admin

from news.models import NewsItem


# Register your models here.
@admin.register(NewsItem)
class NewsItemAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_at']

    search_fields = ['title', 'description']
