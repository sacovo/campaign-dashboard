from modeltranslation.translator import TranslationOptions, translator

from news.models import NewsItem


class NewsItemOptions(TranslationOptions):
    fields = ('title', 'content')


translator.register(NewsItem, NewsItemOptions)
