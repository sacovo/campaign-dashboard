from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from news.models import NewsItem

# Create your views here.


class NewsItemList(ListView):
    paginate_by = 12
    model = NewsItem


class NewsItemDetail(DetailView):
    model = NewsItem
