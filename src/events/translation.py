from modeltranslation.translator import TranslationOptions, translator

from events.models import Category


class CategoryOptions(TranslationOptions):
    fields = ('name', )


translator.register(Category, CategoryOptions)
