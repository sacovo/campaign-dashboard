import uuid

from address.models import AddressField
from ckeditor.fields import RichTextField
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.urls.base import reverse
from django.utils.translation import gettext_lazy as _
from imagefield.fields import ImageField

from events.utils import sign_url

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("category"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Postcode(models.Model):
    zip_code = models.CharField(max_length=10,
                                unique=True,
                                verbose_name=_("zip code"))
    name = models.CharField(max_length=80)
    latitude = models.FloatField()
    longitude = models.FloatField()

    def __str__(self):
        return str(self.zip_code) + " " + self.name


class Event(models.Model):
    DRAFT = 0
    IN_REVIEW = 1
    PUBLISHED = 2
    DELETED = 3

    EVENT_STATES = (
        (DRAFT, _("draft")),
        (IN_REVIEW, _("review")),
        (PUBLISHED, _("published")),
        (DELETED, _("deleted")),
    )
    title = models.CharField(max_length=255, verbose_name=_("title"))

    start_date = models.DateTimeField(verbose_name=_("start date"))
    end_date = models.DateTimeField(verbose_name=_("end date"))

    description = RichTextField(verbose_name=_("description"))

    url = models.URLField(blank=True)

    address = AddressField(blank=True, null=True, verbose_name=_("address"))

    image = ImageField(verbose_name=_("image"),
                       blank=True,
                       auto_add_fields=True,
                       formats={
                           'preview': ['default', ('crop', (1200, 630))],
                           'square': ['default', ('crop', (400, 400))]
                       })

    state = models.IntegerField(choices=EVENT_STATES,
                                default=0,
                                verbose_name=_("state"))

    managers = models.ManyToManyField(get_user_model(),
                                      related_name="managed_events",
                                      blank=True,
                                      verbose_name=_("managers"))
    owner = models.ForeignKey(get_user_model(),
                              models.CASCADE,
                              related_name="owned_events",
                              verbose_name=_("owner"))

    rocket_chat = models.CharField(max_length=45,
                                   blank=True,
                                   verbose_name=_("chat"))

    categories = models.ManyToManyField(Category,
                                        verbose_name=_("categories"),
                                        blank=True)
    is_online = models.BooleanField(default=False, verbose_name=_("is online"))
    is_offline = models.BooleanField(default=False,
                                     verbose_name=_("is offline"))

    internal_id = models.UUIDField(default=uuid.uuid4)
    internal_note = RichTextField(verbose_name=_("internal note"), blank=True)

    @property
    def chat_link(self):
        return settings.ROCKET_CHAT_BASE + self.rocket_chat

    def signed_map_url(self, size):
        return sign_url(self.map_image_url(size))

    def map_image_url(self, size):
        return (
            "https://maps.googleapis.com/maps/api/staticmap"
            f"?markers=color:red|{self.address.latitude},{self.address.longitude}"
            f"&zoom=12&size={size}&key={settings.GOOGLE_API_KEY}")

    def map_url(self):
        return f"https://www.google.com/maps/search/?api=1&query={self.address.latitude},{self.address.longitude}"

    class Meta:
        verbose_name = _("event")
        verbose_name_plural = _("events")
        ordering = ['-start_date']
        permissions = [
            ("publish_event", _("Can publish an event.")),
        ]

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('events:event_detail', args=(self.pk, ))


class AccessRequest(models.Model):
    user = models.ForeignKey(get_user_model(),
                             models.CASCADE,
                             verbose_name=_("user"))
    event = models.ForeignKey(Event, models.CASCADE, verbose_name=_("event"))
    created_at = models.DateTimeField(verbose_name=_("created"),
                                      auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'event'],
                                    name="one_request_per_user")
        ]
