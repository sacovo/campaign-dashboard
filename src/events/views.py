from dal import autocomplete
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.postgres.search import (SearchHeadline, SearchQuery,
                                            SearchRank, SearchVector)
from django.db.models.expressions import F, Value
from django.db.models.query_utils import Q
from django.shortcuts import redirect
from django.urls.base import reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from dashboard.utils import ActionView
from events.actions import (AcceptRequest, DeleteRequest, PublishEvent,
                            RequestAccessAction, UnpublishEvent,
                            can_edit_event)
from events.forms import EventForm, FilterForm
from events.models import AccessRequest, Category, Event, Postcode

# Create your views here.


class EventList(ListView):
    model = Event
    paginate_by = 5

    def get_queryset(self):
        search = self.request.GET.get('search')
        zip_code = self.request.GET.get('zip_code')

        if self.request.user.is_authenticated:
            queryset = super().get_queryset().filter(
                end_date__gte=timezone.now(), )
        else:
            queryset = super().get_queryset().filter(
                state=Event.PUBLISHED,
                end_date__gte=timezone.now(),
            )

        self.filter_form = FilterForm(self.request.GET)
        self.filter_form.is_valid()

        search_data = self.filter_form.cleaned_data

        if search := search_data.get('search'):
            query = SearchQuery(search)
            vector = SearchVector('description', weight="A") + SearchVector(
                'title', weight="B")
            queryset = queryset.annotate(
                rank=SearchRank(vector, query),
                headline=SearchHeadline(
                    'description',
                    query,
                    start_sel="<em>",
                    stop_sel="</em>",
                    max_words=40,
                    min_words=20,
                ),
            ).order_by('-rank')

        online = search_data.get('is_online')
        if online is not None:
            if online:
                queryset = queryset.filter(is_online=True)
            else:
                queryset = queryset.filter(is_offline=True)

        if start_date := search_data.get("start_date"):
            queryset = queryset.filter(end_date__date__gte=start_date)

        if end_date := search_data.get("end_date"):
            queryset = queryset.filter(start_date__date__lte=end_date)

        if code := search_data.get('zip_code'):
            latitude = code.latitude
            longitude = code.longitude

            queryset = queryset.annotate(
                delta_lat=F('address__latitude') - Value(latitude),
                delta_lon=F('address__longitude') - Value(longitude),
            ).annotate(distance=F('delta_lat') * F('delta_lat') +
                       F('delta_lon') * F('delta_lon')).order_by('distance')

        if category := search_data.get('category'):
            queryset = queryset.filter(categories=category)

        return queryset

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        data['filter_form'] = self.filter_form
        if data['page_obj'].has_next():
            get_query = self.request.GET.copy()
            get_query['page'] = data['page_obj'].number + 1

            data['querystring'] = get_query.urlencode()

        return data


class ExternalEventList(EventList):

    template_name = 'events/external_event_list.html'


class EventDetail(DetailView):
    model = Event

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return super().get_queryset().filter(
                end_date__gte=timezone.now(), )
        else:
            return super().get_queryset().filter(
                state=Event.PUBLISHED,
                end_date__gte=timezone.now(),
            )

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        data['title'] = self.get_object().title
        data['can_edit'] = can_edit_event(self.get_object(), self.request.user)

        if data['can_edit']:
            data['access_requests'] = AccessRequest.objects.filter(
                event=self.get_object())

        return data


class ExternalEventDetail(EventDetail):
    template_name = 'events/external_event_detail.html'


class EventAction(ActionView):
    model = Event
    actions = {
        'publish': PublishEvent,
        'unpublish': UnpublishEvent,
        'access': RequestAccessAction,
        'accept': AcceptRequest,
        'decline': DeleteRequest,
    }

    def get_queryset(self):
        return super().get_queryset()


@method_decorator(login_required, name='dispatch')
class EventCreate(SuccessMessageMixin, CreateView):
    model = Event
    success_message = _("%(title)s was created successfully")

    form_class = EventForm

    def dispatch(self, *args, **kwargs):
        response = super().dispatch(*args, **kwargs)

        if response.status_code == 302:
            response['HX-Redirect'] = response.url

        return response

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class EventUpdate(SuccessMessageMixin, UpdateView):
    model = Event
    success_message = _("%(title)s was updated successfully")
    form_class = EventForm

    def get_queryset(self):
        return super().get_queryset().filter(
            Q(owner=self.request.user)
            | Q(managers=self.request.user))


@method_decorator(login_required, name='dispatch')
class EventDelete(SuccessMessageMixin, DeleteView):
    model = Event

    success_message = _("%(title)s was deleted")
    success_url = reverse_lazy('events:event_list')


@permission_required('events.publish_event')
def publish_event(request, pk):
    if request.method == 'POST':
        Event.objects.filter(pk=pk).update(state=Event.PUBLISHED)
    return redirect('events:event_detail', pk)


class CategoryAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if self.q:
            return Category.objects.filter(name__icontains=self.q)

        return Category.objects.all()[:30]


class PostcodeAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if self.q:
            return Postcode.objects.filter(
                Q(zip_code__startswith=self.q)
                | Q(name__icontains=self.q))[:30]
        return Postcode.objects.none()
