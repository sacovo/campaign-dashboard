from ckeditor.fields import RichTextFormField
from dal import autocomplete
from django import forms
from django.utils.translation import gettext as _
from html_sanitizer.django import get_sanitizer

from events.models import AccessRequest, Category, Event, Postcode


class EventForm(forms.ModelForm):
    def clean_description(self):
        return get_sanitizer().sanitize(self.cleaned_data['description'])

    def clean_internal_note(self):
        return get_sanitizer().sanitize(self.cleaned_data['internal_note'])

    class Meta:
        model = Event

        fields = [
            'title',
            'start_date',
            'end_date',
            'description',
            'url',
            'is_online',
            'is_offline',
            'address',
            'image',
            'managers',
            'categories',
            'rocket_chat',
        ]

        widgets = {
            'start_date':
            forms.SplitDateTimeWidget(
                date_format="%Y-%m-%d",
                date_attrs={'type': 'date'},
                time_attrs={'type': 'time'},
            ),
            'end_date':
            forms.SplitDateTimeWidget(
                date_format="%Y-%m-%d",
                date_attrs={'type': 'date'},
                time_attrs={'type': 'time'},
            ),
            'managers':
            autocomplete.ModelSelect2Multiple(
                url="dashboard:user-autocomplete"),
            'categories':
            autocomplete.ModelSelect2Multiple(
                url="events:category_autocomplete", )
        }

        field_classes = {
            'start_date': forms.SplitDateTimeField,
            'end_date': forms.SplitDateTimeField,
        }


class SelectAcceptRequestForm(forms.Form):
    access_request = forms.ModelChoiceField(AccessRequest.objects.all(), )


class FilterForm(forms.Form):
    search = forms.CharField(required=False)
    is_online = forms.NullBooleanField(
        label=_("type"),
        required=False,
        widget=forms.Select(choices=[
            ('', '--'),
            (True, _('online')),
            (False, _('offline')),
        ]),
    )
    start_date = forms.DateField(required=False,
                                 label=_("after"),
                                 widget=forms.DateInput(format="%Y-%m-%d",
                                                        attrs={'type':
                                                               'date'}))
    end_date = forms.DateField(required=False,
                               label=_("before"),
                               widget=forms.DateInput(format="%Y-%m-%d",
                                                      attrs={'type': 'date'}))
    zip_code = forms.ModelChoiceField(
        queryset=Postcode.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="events:postcode_autocomplete"),
    )

    category = forms.ModelChoiceField(
        queryset=Category.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(url="events:category_autocomplete"),
    )

    def clean(self):
        if self.cleaned_data['start_date'] and self.cleaned_data[
                'end_date'] and self.cleaned_data[
                    'start_date'] > self.cleaned_data['end_date']:
            self.add_error('start_date',
                           _("start date has to be before end date"))
