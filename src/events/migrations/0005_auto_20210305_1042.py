# Generated by Django 3.1.7 on 2021-03-05 10:42

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20210304_1744'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='event',
            name='internal_note',
            field=ckeditor.fields.RichTextField(blank=True,
                                                verbose_name='internal note'),
        ),
    ]
