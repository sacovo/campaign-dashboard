from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.forms.forms import Form
from django.http.response import JsonResponse
from django.shortcuts import redirect
from django.utils.translation import gettext as _

from dashboard.utils import LoginRequiredAction
from events.forms import SelectAcceptRequestForm
from events.models import AccessRequest, Event


def can_edit_event(event, user):
    return (event.owner == user or user in event.managers.all())


class PublishEvent(LoginRequiredAction):
    def form_valid(self, form):
        event: Event = self.obj
        if not can_edit_event(event, self.request.user):
            raise PermissionDenied("not allowed to publish %s",
                                   params={'event': event})
        event.state = Event.PUBLISHED
        event.save()
        messages.add_message(self.request, messages.SUCCESS,
                             _("Event published."))

        return redirect(event.get_absolute_url())


class UnpublishEvent(LoginRequiredAction):
    def form_valid(self, form):
        event: Event = self.obj
        if not can_edit_event(event, self.request.user):
            raise PermissionDenied("not allowed to unpublish %s",
                                   params={'event': event})
        event.state = Event.DRAFT
        event.save()
        messages.add_message(self.request, messages.SUCCESS,
                             _("Event unpublished."))
        return redirect(event.get_absolute_url())


class AcceptRequest(LoginRequiredAction):
    form = SelectAcceptRequestForm

    def form_valid(self, form):
        event: Event = self.obj
        if not can_edit_event(event, self.request.user):
            raise PermissionDenied("not allowed to grant access to %s",
                                   params={'event': event})
        access_request = self.data['access_request']

        if not event.pk == access_request.event_id:
            raise PermissionDenied("wrong event")

        event.managers.add(access_request.user)
        messages.add_message(self.request, messages.SUCCESS,
                             _("Access granted."))
        access_request.delete()

        return redirect(event.get_absolute_url())


class DeleteRequest(LoginRequiredAction):
    form = SelectAcceptRequestForm

    def form_valid(self, form):
        event: Event = self.obj
        if not can_edit_event(event, self.request.user):
            raise PermissionDenied("not allowed to grant access to %s",
                                   params={'event': event})
        access_request = self.data['access_request']

        if not event.pk == access_request.event_id:
            raise PermissionDenied("wrong event")
        access_request.delete()
        return redirect(event.get_absolute_url())


class RequestAccessAction(LoginRequiredAction):
    def form_valid(self, form):
        event: Event = self.obj
        AccessRequest.objects.get_or_create(event=event,
                                            user=self.request.user)
        messages.add_message(self.request, messages.SUCCESS,
                             _("Access request sent."))
        return redirect(event.get_absolute_url())
