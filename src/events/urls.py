from django.urls import path

from events.views import (CategoryAutoComplete, EventAction, EventCreate,
                          EventDelete, EventDetail, EventList, EventUpdate,
                          PostcodeAutoComplete)

app_name = 'events'

urlpatterns = [
    path("categories/",
         CategoryAutoComplete.as_view(),
         name="category_autocomplete"),
    path("postcodes/",
         PostcodeAutoComplete.as_view(),
         name="postcode_autocomplete"),
    path("create/", EventCreate.as_view(), name="event_create"),
    path("<int:pk>/", EventDetail.as_view(), name="event_detail"),
    path("<int:pk>/action/", EventAction.as_view(), name="event_action"),
    path("<int:pk>/update/", EventUpdate.as_view(), name="event_update"),
    path("<int:pk>/delete/", EventDelete.as_view(), name="event_delete"),
    path("", EventList.as_view(), name="event_list"),
]
