from django.urls.conf import path

from events.views import ExternalEventDetail, ExternalEventList

app_name = "external_events"

urlpatterns = [
    path("<int:pk>/", ExternalEventDetail.as_view(), name="event_detail"),
    path("", ExternalEventList.as_view(), name="event_list"),
]
