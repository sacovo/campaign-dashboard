from django import template

register = template.Library()


@register.filter(name="staticmap")
def static_map(event, size="400x400"):
    return event.signed_map_url(size)
