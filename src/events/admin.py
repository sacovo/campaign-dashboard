from django.contrib import admin

from events.models import Category, Event, Postcode

# Register your models here.


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    filter_horizontal = ['managers']

    list_display = ['title', 'start_date', 'state', 'owner', 'rocket_chat']

    search_fields = ['title', 'description']

    list_filter = ['categories']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


@admin.register(Postcode)
class PostcodeAdmin(admin.ModelAdmin):
    pass
